var client = ZAFClient.init();
var comment = [], bttnArray = []; tlfArray = []
var UrlBase, dataMailId, telf;
var entrar = true, EventkeyPress = false;
var emailPaciente, telefonoPaciente, ticketId;
$(function() {
    client.invoke('resize', { width: '100%', height: '310px' });
    client.get('ticket.id').then(function(data) {
        ticketId = data["ticket.id"];
    });
    client.metadata().then(function(metadata) {
        UrlBase = metadata.settings.token;
    });
    client.get('ticket.requester').then(function(data) {
        dataMailId = data["ticket.requester"].email; 
        if ( data["ticket.requester"].email ) {DoMail(data["ticket.requester"].email, false);}
        else if ( data["ticket.requester"].telephone ) {DoTelf(data["ticket.requester"].telephone)}
    });
});
    function DoMail(item, value) {
        LoadTemplate('load', item , 'labels');
        var settings = {
            url: 'https://patients-customer-support-rest-api.saludonnet.com/v1/contact/emails?email=' + item,
            type: 'GET',
            headers: {"Authorization" : "Basic " + UrlBase },
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
        };
        client.request(settings).then(
            function (data) {
                var comm = [data];
                DoChanges(comm, value);
            },
            function (response) {
                if ( EventkeyPress ) {
                    var arr = [];
                    arr = { mail : item };
                    LoadTemplate('errorMail', arr , 'labels' );
                    EventkeyPress = false;
                }
                else if (!emailPaciente) {GetComments('email')}
                else if (emailPaciente && !telefonoPaciente ) {GetComments('telef')}
                }  
        );
    }
    function GetComments(elem) {
        var settings = {
            url: 'https://saludonnet.zendesk.com/api/v2/tickets/'+ ticketId + '/comments.json',
            headers: {"Authorization" : "Basic " + UrlBase },
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
        };
        client.request(settings).then(
            function (data) {
                comment = data;
                GetCommentValue(elem);
            },
            function (response) { 
                console.log('Ha surgido un error', response);
                }  
        );
    }
    function GetCommentValue(elem){
        var str = comment.comments[0].html_body.split(">");
        for ( var x = 0; x < str.length; x++) {
            if ( elem === 'email'){
                if ( str[x].includes('@')){
                    if ( entrar ) {
                        emailPaciente = str[x].split("<")[0];
                        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                        if(re.test(emailPaciente)) {
                            DoMail(emailPaciente, false);
                            entrar = false;
                        }
                        else { 
                            console.log('entra aquiii 1 ??')
                            entrar = false; 
                            var arr = [];
                            arr = { mail : dataMailId };
                            LoadTemplate('errorMail', arr , 'labels' )}
                    }
                    entrar = true;
                }
                else if (str.length == x && !str[x].includes('@')){
                    console.log('entra aquiii 2 ??')
                    var arr = [];
                    arr = { mail : dataMailId };
                    LoadTemplate('errorMail', arr , 'labels' )
                }
            }
            else if ( elem === 'telef' ){
                if ( str[x].startsWith('6')){
                    telefonoPaciente = str[x].split("<")[0];
                    DoTelf(telefonoPaciente);
                }
            }
        }
    }
    function DoTelf(item) {
        var settings = {
            url: 'https://patients-customer-support-rest-api.saludonnet.com/v1/contact/telephones?telephone=' + item,
            type: 'GET',
            headers: {"Authorization" : "Basic " + UrlBase },
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
        };
        client.request(settings).then(
            function (data) {
                comment = data;
                tlfArray = data;
                telf = item;
                DoChanges(data, false);
            },
            function (response) {
                if ( EventkeyPress ) {
                    var arr = [];
                    arr = { tlf : item };
                    LoadTemplate('errorTlf', arr , 'labels' );
                    EventkeyPress = false;
                }
                var arr = [];
                arr = { tlf : item };
                LoadTemplate('errorTlf', arr, 'labels' )
                }  
        );
    }  
    function LoadTemplate(template, obj, idCont) {
        var source = $("#" + template + "-template").html();
        var template = Handlebars.compile(source);
        var html = template(obj);
        $('#' + idCont).html("");
        $('#' + idCont).html(html);
    }
    function DoChanges(data, value) {
        for(var x = 0; x < data.length; x++){
            var birth = moment(data[x].birthdate).format('MMMM DD YYYY');
            var signUp = moment(data[x].signUpDate).format('MMMM DD YYYY');
            if ( data[x].agreesToReceiveNotifications ) { data[x].agreesToReceiveNotifications = 'Si' } else { data[x].agreesToReceiveNotifications = 'No' };
            if ( data[x].sex === 'Male' ) { data[x].sex = 'Hombre' } else { data[x].sex = 'Mujer' }; 
            for (var y = 0; y < data[x].telephones ; y++) {
                data[x].telephones[y] = { tlf : data[x].telephones[y] }
            }
            data[x].birthdate = birth;
            data[x].signUpDate = signUp;
        }   
        comment = { contacts : data };
        bttnArray = [comment];
        if(data.length === 1 && value === false) {
            LoadTemplate('panelLabel', data[0], 'labels' );
        }
        else if (data.length === 1 & value === true) {
            data[0].value = true;
            LoadTemplate('panelLabel', data[0], 'labels' );
        }
        else if ( bttnArray[0].contacts.length > 1 ) {
            for ( var x = 0; x < bttnArray[0].contacts.length ; x++) {
                bttnArray[0].contacts[x].value = x;
            }
            LoadTemplate('selector', bttnArray[0], 'labels');
            $('.btn').click( function () {
                var name = this.name;
                for ( var x = 0; x < bttnArray[0].contacts.length ; x++) {
                    if ( bttnArray[0].contacts[x].value == name ){
                        DoMail(bttnArray[0].contacts[x].email, true);
                    }
                }
            });
        }
    }
    
    function DoEvent(elem) {
        var value = $(elem).val();
        $(elem).val("");
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(re.test(value)) {
            DoMail(value, false);
        }
        else {
            var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
            if ( value.match(/\d/g).length===9 ){
                DoTelf(value);
            }
            else if ( re.test(value) ) {
                if ( value.includes('+')){
                    DoTelf(value.split(value.match(/.{3}/)[0])[1]);
                }
                else {DoTelf(value);}
            }
            else {
                var arr = [];
                arr = { tlf : value };
                LoadTemplate('errorTlfPress', arr , 'labels' );
            }
        }
    }

    function Return() {
        DoChanges(tlfArray, false);
    }

    function createContact(item) {
        console.log('item', item);
    }

    $('.search__input').keypress( function (e) {
        if (e.which == '13') {
            LoadTemplate('load', '' , 'labels' );
            EventkeyPress = true;
            DoEvent(this);
        }
    });